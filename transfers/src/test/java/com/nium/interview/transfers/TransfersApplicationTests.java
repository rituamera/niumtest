package com.nium.interview.transfers;

import com.nium.interview.transfers.model.Account;
import com.nium.interview.transfers.model.Transfer;
import org.assertj.core.api.Condition;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
	@RunWith(MockitoJUnitRunner.class)
	class TransfersApplicationTests {

		@Before
		public void initMocks() {
			MockitoAnnotations.initMocks(this);
		}

		@BeforeEach
		public void setUp() {

		}

		final URL file = getClass().getClassLoader().getResource("transfers.txt");

		@Test
		void testReadFileSize() {
			// Test if returned List of String has the same size as the number of lines from txt file.
			assertThat(TransfersApplication.readFile(file)).hasSize(9);

		}

		@Test
		void testReadFileContent() {
			// Test if returned List of String is the same with the txt file content.
			assertThat(TransfersApplication.readFile(file)).containsAll(List.of("# Date: 15/08/2055",
					"SOURCE_ACCT, DESTINATION_ACCT, AMOUNT, DATE, TRANSFERID",
					"0, 112233, 60.00, 10/08/2055, 1445",
					"0, 223344, 25.03, 10/08/2055, 1446",
					"0, 334455, 67.67, 11/08/2055, 1447",
					"112233, 223344, 11.11, 11/08/2055, 1448",
					" 112233, 334455, 12.12, 13/08/2055,   1449",
					"223344, 334455, 006.018, 13/08/2055, 1450"));
		}

		@Test
		public void testGetAccountWithHighestBalanceNull() {
			try {
				Optional<Account> expectedValue = Optional.empty();

				List<Account> accounts = null;
				Optional<Account> actualValue = TransfersApplication.getAccountWithHighestBalance(accounts);

				assertEquals(expectedValue, actualValue);

			} catch (Exception exception) {
				exception.printStackTrace();
				assertFalse(false);
			}
		}

		@Test
		public void testGetAllDistinctAccounts() {
			List<Account> expectedAccounts = List.of(new Account(112233, 60.00),
					new Account(223344, 25.03),
					new Account(334455, 67.67));
			Date date =  new Date(2045,02,11);
			List<Transfer> transfers = List.of(new Transfer(0,112233, 60.00, date,1445),
					new Transfer(0,223344, 25.03, date,1446),
					new Transfer(0,334455, 67.67, date,1447));
			List<Account> actualAccounts = TransfersApplication.getAllDistinctAccounts(transfers);
			assertEquals(expectedAccounts.get(0).getAccountId(),actualAccounts.get(0).getAccountId());
			assertEquals(expectedAccounts.get(1).getAccountId(),actualAccounts.get(1).getAccountId());
			assertEquals(expectedAccounts.get(0).getBalance(),actualAccounts.get(0).getBalance());
			assertEquals(expectedAccounts.get(1).getBalance(),actualAccounts.get(1).getBalance());
		}


	}
