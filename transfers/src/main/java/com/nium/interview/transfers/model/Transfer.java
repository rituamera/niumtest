package com.nium.interview.transfers.model;

import lombok.*;

import java.util.Date;

/**
 * <p>
 *   The <b>Transfer</b> Class.
 * </p>
 *   This Class will contain all the details related to the transfer made.
 * <p> It is an Object representation of a line from the transfers.txt file. </p>
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Transfer {
  
  /** The SOURCE_ACCT field. */
  private Integer sourceAccount;
  
  /** The DESTINATION_ACCT field. */
  private Integer destinationAccount;
  
  /** The AMOUNT field. */
  private Double amount;
  
  /** The DATE field. */
  private Date date;
  
  /** The TRANSFERID field. */
  private Integer transferId;

}
