package com.nium.interview.transfers.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>
 *   The <b>Account</b> Class.
 * </p>
 *   This Class will contain account details:
 * <p> Account Id, and the current balance. </p>
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account {
  
  /** The account Id field. */
  private Integer accountId;
  
  /** The current balance field. */
  private Double balance;
}
