package com.nium.interview.transfers;

import com.nium.interview.transfers.model.Account;
import com.nium.interview.transfers.model.Transfer;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 	A Command-line application to parse and process a transfers file and provide the business requirements, namely:
 * 	<ul>
 * 	    <li>1. Print the final balances on all bank accounts</li>
 * 	    <li>2. Print the bank account with the highest balance</li>
 * 	    <li>3. Print the most frequently used source bank account</li>
 * 	</ul>
 * </p>
 */
@SpringBootApplication
@Log4j2
public class TransfersApplication implements CommandLineRunner {


	public static void main(String[] args) {
		SpringApplication.run(TransfersApplication.class, args);
	}

	@Override
	public void run(final String... args) {
		// Below is some sample code to get you started. Good luck :)

		//final URL file = getClass().getClassLoader().getResource("transfers.txt");

		// First step, read all lines from .txt file and store them into a List of Strings.
		final URL file = getClass().getClassLoader().getResource("transfers.txt");
		List<String> lines = readFile(file);

		// Next step, parse/convert/map this List of Strings, where each element represents a line from the txt file into Transfer Object,skipped first 3 lines
		if(!lines.isEmpty()) {
			List<Transfer> transfers = lines.stream()
					.skip(3)
					.map(TransfersApplication::convertLineToTransferObject)
					.collect(Collectors.toList());

			/*Next step, we will execute each transfer from the txt file, We add all accounts, and execute all transfers between them, updating balance for each account.*/
			List<Account> accounts = executeTransfers(transfers);

			// And now we will print the results

			log.info("1. Print the final balances on all bank accounts:");
			for(Account account : accounts) {
				log.info(String.format("  AccountId: %d, final balance: %.3f", account.getAccountId(), account.getBalance()));
			}

			log.info("2. Print the bank account with the highest balance:");
			Optional<Account> optAccountHighestBalance = getAccountWithHighestBalance(accounts);
			if(optAccountHighestBalance.isPresent()) {
				Account accountHighestBalance = optAccountHighestBalance.get();
				log.info(String.format("  AccountId: %d, balance: %.3f", accountHighestBalance.getAccountId(), accountHighestBalance.getBalance()));
			}

			log.info("3. Print the most frequently used source bank account:");
			Integer mostFrequentlyUsedSourceAccount = getMostFrequentlyUsedSourceAccountId(transfers);
			if(mostFrequentlyUsedSourceAccount != null) {
				log.info("  sourceAccountId:" + mostFrequentlyUsedSourceAccount);
			}

		} else {
			log.info("No lines found.");
		}
	}


	/** Read .txt file, and store all lines in a List of String,
	 * where each item will hold the String value of a line.
	 * @return List of String - holding all readed lines. 	 */
	public static List<String> readFile(URL filePath) {
		List<String> lines = new ArrayList<>();
		try {
			lines = Files.readAllLines(Path.of(filePath.toURI()));
		} catch (IOException e) {
			log.error("Error occurred while trying to read from the file: " + e);
		} catch (URISyntaxException e) {
			log.error("Error occurred while trying to parse file path to URI: " + e);
		}
		return lines;
	}

	/** Construct/Convert the line from the txt file into a Transfer object,
	 * with all properties/fields mapped.
	 * @return Transfer object. */
	public static Transfer convertLineToTransferObject(String line) {
		String[] values = line.replaceAll("\\s","").split(",");
		Transfer object = new Transfer();
		object.setSourceAccount(Integer.valueOf(values[0]));
		object.setDestinationAccount(Integer.valueOf(values[1]));
		object.setAmount(Double.valueOf(values[2]));
		object.setDate(convertStringToDate((values[3])));
		object.setTransferId(Integer.valueOf(values[4]));
		return object;
	}

	/** Convert String date to Date object.  dateAsString - String read from .txt file
	 * @return Date */
	public static Date convertStringToDate(String dateAsString) {
		try {
			return new SimpleDateFormat("dd/MM/yyyy").parse(dateAsString);
		} catch (Exception e) {
			log.error("Error occurred while trying to parse String to Date: " + e);
		}
		return null;
	}

	/** Execute all transfers from txt file.
	 * If source account = 0, assumption made of adding funds on account from external sources.
	 *  Else, if source account is an existing account, the transfer between them is made. */
	public static List<Account> executeTransfers(List<Transfer> transfers) {
		// First we will add all distinct accounts and funds from the source account 0.
		List<Account> accounts = getAllDistinctAccounts(transfers.stream()
				.filter(transfer -> transfer.getSourceAccount().equals(0))
				.collect(Collectors.toList()));

		// Now we will execute the other transfers between existing accounts
		if(!CollectionUtils.isEmpty(transfers)) {
			for(Transfer transfer: transfers) {
				if(!transfer.getSourceAccount().equals(0)) {
					accounts.stream()
							.filter(acc -> acc.getAccountId().equals(transfer.getSourceAccount()))
							.findFirst().ifPresent(acc -> acc.setBalance(acc.getBalance() - transfer.getAmount()));

					accounts.stream()
							.filter(acc -> acc.getAccountId().equals(transfer.getDestinationAccount()))
							.findFirst().ifPresent(acc -> acc.setBalance(acc.getBalance() + transfer.getAmount()));
				}
			}
		}
		return accounts;
	}

	/** Get a List of all distinct Accounts found in transfers.
	 * @return List of Account, all distinct accounts found in transfers. */
	public static List<Account> getAllDistinctAccounts(List<Transfer> transfers){
		List<Account> accounts = new ArrayList<>();
		if(!CollectionUtils.isEmpty(transfers)) {
			for(Transfer transfer: transfers) {
				Account newAccount = new Account();
				newAccount.setAccountId(transfer.getDestinationAccount());
				newAccount.setBalance(transfer.getAmount());
				if(checkIfAccountExists(accounts, newAccount) == false) {
					accounts.add(newAccount);
				}
			}
		}
		return accounts;
	}

	/** Check if account already exist in accounts list.
	 * @return true, if account already exists, false otherwise. 	 */
	public static boolean checkIfAccountExists(List<Account> accounts, Account account) {
		return accounts.stream()
				.filter(acc -> acc.getAccountId().equals(account.getAccountId()))
				.findFirst()
				.isEmpty() ? false : true;
	}


	/** To get the account with the
	 * highest balance.
	 * @param accounts
	 * @return Optional of Account with the highest balance.
	 */
	public static Optional<Account> getAccountWithHighestBalance(List<Account> accounts) {
		if(accounts != null) {
			return accounts.stream().max(Comparator.comparing(Account::getBalance));
		}
		return Optional.empty();
	}

	/** To get the most frequently used source bank account id.
	 * @return Most frequently used source bank account id. 	 */
	public static Integer getMostFrequentlyUsedSourceAccountId(List<Transfer> transfers) {
		List<Integer> sourceAccountsIds = transfers.stream()
				.filter(t -> !t.getSourceAccount().equals(0))
				.map(Transfer::getSourceAccount)
				.collect(Collectors.toList());

		int maxFrequency = 0;
		Integer mostFrequentlyUsedSourceAccount = null;
		for(Integer sourceAccountId : sourceAccountsIds) {
			int currentSourceAccountIdFrequency = Collections.frequency(sourceAccountsIds, sourceAccountId);
			if(currentSourceAccountIdFrequency > maxFrequency) {
				maxFrequency = currentSourceAccountIdFrequency;
				mostFrequentlyUsedSourceAccount = sourceAccountId;
			}
		}
		return mostFrequentlyUsedSourceAccount;
	}
}
